add_library(TensorRT::nvinfer_plugin SHARED IMPORTED)
set_target_properties(
	TensorRT::nvinfer_plugin PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "/usr/include"
	IMPORTED_LOCATION "/usr/lib/@DEB_HOST_MULTIARCH@/libnvinfer_plugin.so"
)
